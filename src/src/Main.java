import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Main {
    static List<Product> productsList = new ArrayList<>();

    public static void main(String[] args) {
        CommonFunctions commonFunctions = new CommonFunctions();
        productsList = commonFunctions.setProductsList();
        Random rand = new Random();

        //Consumer
        //to print the product to appropriate medium depending on the print parameter. If the print parameter is set to file, consumer shall log the product to file, if not print on the console.
        //Need clarity as Consumer will accept only one parameter

        //update the grade of the product as 'Premium' if the price is > 1000/-. Given the product list, update the grade for each product and print the products.
        System.out.println("Consumer 2");
        Consumer<Product> updateGradeOfTheProduct = product -> {
            if(product.productPrice > 1000){
                product.productCategory = "Premium";
                System.out.println(product);
            }
        };
        for (Product p:productsList) {
            updateGradeOfTheProduct.accept(p);
        }

        //update the name of the product to be suffixed with '*' if the price of product is > 3000/-.
        System.out.println("Consumer 3");
        Consumer<Product> updateNameOfTheProduct = product -> {
            if(product.productPrice > 3000){
                product.productName = "*"+product.productName;
                System.out.println(product);
            }
        };
        for (Product p:productsList) {
            updateNameOfTheProduct.accept(p);
        }

        //update the name of the product to be suffixed with '*' if the price of product is > 3000/-.
        System.out.println("Consumer 4");
        Consumer<Product> displayProductMatching = product -> {
            if(product.productCategory.equalsIgnoreCase("Premium") && product.productPrice > 3000){
                System.out.println(product);
            }
        };
        for (Product p:productsList) {
            displayProductMatching.accept(p);
        }

        //Supplier
        System.out.println("Supplier 1");
        Supplier<Product> getRandomProduct = () -> productsList.get(rand.nextInt(productsList.size()));
        System.out.println("Random product from Supplier : "+getRandomProduct.get().toString());

        System.out.println("Supplier 2");
        Supplier<Integer> getOTP = () -> rand.nextInt(10000);
        System.out.println("OTP from Supplier : "+getOTP.get());
    }
}